import { createApp } from 'vue'
import App from './App.vue'
import tooltips from './directive/tooltips'
import loading from './directive/loading'

// import 'ant-design-vue/dist/antd.css'
import {
  List,
  Button,
  Input,
  Checkbox,
  Radio,
  Popconfirm,
  Tooltip,
} from 'ant-design-vue'

const antdComponents: AnyType[] = [
  List,
  Button,
  Input,
  Checkbox,
  Radio,
  Popconfirm,
  Tooltip,
]

const app = createApp(App)
antdComponents.forEach((component) => {
  app.use(component)
})
// directive
app.use(tooltips)
app.use(loading)

app.mount('#app')
