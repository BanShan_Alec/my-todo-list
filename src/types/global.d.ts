type CommonObject = { [key: string]: any }
type AnyType = any

// npm js包
declare module 'sortablejs'
declare module 'lodash.debounce'
// request
declare module '@/request/index'