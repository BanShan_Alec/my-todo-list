import { Tooltip } from 'ant-design-vue'
import { createApp, h, ref } from 'vue'

const tooltipVisible = ref(false)

export default {
  install(app: AnyType) {
    app.directive('tooltip', {
      // el 是真实dom
      mounted(el: HTMLElement, binding: AnyType) {
        const { value: content, arg: placement } = binding
        // const content = binding.value
        // const placement = binding.arg
        const ToolTip = createApp({
          render() {
            return h(
              Tooltip,
              {
                placement,
                visible: tooltipVisible.value,
              },
              {
                title: () => content,
              },
            )
          },
        })
        const tooltipsEL = document.createElement('tooltip')
        // mount，就是吧el的innerHTML 替换为ToolTip
        ToolTip.mount(tooltipsEL)

        // 添加tooltipsEL，并修改tooltipsEL样式
        el.appendChild(tooltipsEL)
        el.style.position = 'relative'
        tooltipsEL.style.position = 'absolute'
        tooltipsEL.style.zIndex = '-1'
        tooltipsEL.style.top = '0'
        tooltipsEL.style.left = '0'
        tooltipsEL.style.width = '100%'
        tooltipsEL.style.height = '100%'
        // hover事件
        el.addEventListener('mouseenter', () => {
          tooltipVisible.value = true
        })
        el.addEventListener('mouseleave', () => {
          tooltipVisible.value = false
        })
      },
    })
  },
}
