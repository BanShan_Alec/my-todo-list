import { createApp, h } from 'vue'
import MyLoading from '@/components/MyLoading.vue'

const toggleLoading = async (el: HTMLElement, LoadingVisible: boolean) => {
  const myLoadingEL = el.getElementsByTagName('loading')[0] as HTMLElement
  LoadingVisible
    ? (myLoadingEL.style.display = 'block')
    : (myLoadingEL.style.display = 'none')
}

export default {
  install(app: AnyType) {
    app.directive('loading', {
      // el 是真实dom
      mounted(el: HTMLElement, binding: AnyType) {
        const { value: visible } = binding
        const Loading = createApp({
          render() {
            return h(MyLoading)
          },
        })
        const loadingEL = document.createElement('loading')
        // mount，就是吧el的innerHTML 替换为ToolTip
        Loading.mount(loadingEL)

        // 添加tooltipsEL，并修改tooltipsEL样式
        el.appendChild(loadingEL)
        // el.style.position = 'relative'
        toggleLoading(el, visible)
      },
      updated(el: any, binding: { value: boolean; oldValue: boolean }) {
        if (binding.oldValue !== binding.value) {
          toggleLoading(el, binding.value)
        }
      },
    })
  },
}
