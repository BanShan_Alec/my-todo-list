// import文件 不能以ts结尾
import server from './server.js'

class MyServer {
  constructor() {
    this.server = server
  }

  registerApi(moduleName, apiObject) {
    this[moduleName] = {}
    // apiObject:
    // {
    //   loginIn: { type: "post", url: "/api/loginIn" },
    //   loginOut: { type: "get", url: "/api/loginOut" }
    // }
    for (const [apiName, apiObj] of Object.entries(apiObject)) {
      this[moduleName][apiName] = this.sendMsg.bind(
        this,
        moduleName,
        apiName,
        apiObj.type,
        apiObj.url,
      )
    }
  }

  // 执行请求
  sendMsg(moduleName, apiName, type, url, config = {}) {
    // const self = this
    const { data = {} } = config

    // 分模块：对于请求响应数据的处理分成多个
    const before = function (res) {
      console.log(moduleName, apiName)
      // 适合：效果模块处理(loading)
      return res
    }
    const defaultSuccess = function (res) {
      // 适合：数据处理
      return res
    }
    const defaultError = function (error) {
      // 适合：错误处理
      // console.log(error)
      return error
    }

    // 执行请求
    return this.server[type](url, data)
      .then(before)
      .then(config.success ?? defaultSuccess)
      .catch(defaultError)

    // try {
    //   this.server[type](url, data)
    //     .then(before)
    //     .then(config.success ?? defaultSuccess)
    // } catch (e) {
    //   defaultError(e)
    // }
  }
}

export default new MyServer()
