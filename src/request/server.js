import axios from 'axios'
import { message } from 'ant-design-vue'
const server = axios.create({
  // baseURL: 'http://127.0.0.1:8888/api/private/v1/',
  timeout: 3000,
})

//  请求拦截器
server.interceptors.request.use(
  function (config) {
    return config
  },
  function (err) {
    return Promise.reject(err)
  },
)

// 响应拦截器
server.interceptors.response.use(
  function (res) {
    //  接收响应前,过滤掉除res.data以外的
    if (res.status !== 200) {
      return message.warning(res.statusText)
    }
    return res.data
  },
  function (err) {
    // 对响应错误做点什么
    return Promise.reject(err)
  },
)

// function getDefaultHeader() {
//   return {
//     'Content-Type': 'application/json;charset=utf8',
//     withCredentials: true,
//     timeout: 3000,
//     // 为请求头，添加token验证的Authorization字段
//     authorization: window.sessionStorage.getItem('token') ?? null,
//     // user: Cookies.get(USER_INFO)
//     //   ? JSON.parse(Cookies.get(USER_INFO)).username
//     //   : null,
//     // groupid: currentGroup.groupId,
//     // groupname: currentGroup?.groupName || '',
//   }
// }

export default server
