import { ref, watch } from 'vue'
import debounce from 'lodash.debounce'

type SerializerType =
  | 'boolean'
  | 'object'
  | 'number'
  | 'any'
  | 'string'
  | 'map'
  | 'set'

interface Serializer<T> {
  read(raw: string): T
  write(value: T): string
}

const storageSrializer: Record<SerializerType, Serializer<any>> = {
  boolean: {
    read: (v: any) => v === 'true',
    write: (v: any) => String(v),
  },
  object: {
    read: (v: any) => JSON.parse(v),
    write: (v: any) => JSON.stringify(v),
  },
  number: {
    read: (v: any) => Number.parseFloat(v),
    write: (v: any) => String(v),
  },
  any: {
    read: (v: any) => v,
    write: (v: any) => String(v),
  },
  string: {
    read: (v: any) => v,
    write: (v: any) => String(v),
  },
  map: {
    read: (v: any) => new Map(JSON.parse(v)),
    write: (v: any) =>
      JSON.stringify(Array.from((v as Map<any, any>).entries())),
  },
  set: {
    read: (v: any) => new Set(JSON.parse(v)),
    write: (v: any) => JSON.stringify(Array.from((v as Set<any>).entries())),
  },
}

export default (key: string, initData: CommonObject) => {
  // 响应式
  const data = ref(initData)
  const storage = window.localStorage
  // 目前data的类型写死object
  const serializer = storageSrializer.object
  const read = (event?: StorageEvent) => {
    if (!storage || (event && event.key !== key)) return

    try {
      const rawValue = event ? event.newValue : storage.getItem(key)
      console.log(rawValue)

      if (rawValue === null) {
        // 初始化localStorage,赋默认值
        data.value = initData
        initData !== null && storage.setItem(key, serializer.write(initData))
      } else if (typeof rawValue !== 'string') {
        data.value = rawValue
      } else {
        data.value = serializer.read(rawValue)
      }
    } catch (e) {
      console.log(e)
    }
  }

  // 初始化localStorage,赋默认值
  read()
  window.addEventListener('storage', (e) => setTimeout(() => read(e), 0), false)
  watch(
    data,
    debounce((value: any) => {
      try {
        value === null
          ? storage.removeItem(key)
          : storage.setItem(key, serializer.write(value))
      } catch (e) {
        console.log(e)
      }
      console.log(storage.getItem(key))
    }, 800),
    {
      deep: true,
    },
  )

  return data
}
