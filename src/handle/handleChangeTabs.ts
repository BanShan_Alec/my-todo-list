import { computed, ref } from 'vue'

const listType = ref('all')

const handleChangeTabs = (state: CommonObject) => {
  const handleRadio = (type: string) => (listType.value = type)

  const list = computed({
    get: () => {
      if (listType.value === 'all') return state.value.todoList
      if (listType.value === 'undone') {
        return state.value.todoList.filter((item: CommonObject) => !item.done)
      }
      if (listType.value === 'done') {
        return state.value.todoList.filter((item: CommonObject) => item.done)
      }
      return state.value.todoList
    },
    set: () => console.log('changeList'),
  })
  return {
    handleRadio,
    list,
    listType,
  }
}

export default handleChangeTabs
