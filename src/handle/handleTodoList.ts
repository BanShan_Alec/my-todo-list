import { message } from 'ant-design-vue'
import { computed, ref } from 'vue'

const lastTopIndex = ref(-1)
const tooltipVisible = ref(false)

const handleTodoList = (state: CommonObject) => {
  const UnDoCount = computed(() =>
    state.value.todoList.reduce((sum: number, item: CommonObject) => {
      return !item.done ? sum + 1 : sum
    }, 0),
  )

  const handleCheckbox = (item: CommonObject): void => {
    item.done = !item.done
  }

  const handleAddItem = (info: string): void => {
    state.value.todoList.push({
      id: state.value.nextId++,
      info,
      done: false,
    })
  }

  const handelTooltip = (visible: boolean) => (tooltipVisible.value = visible)

  const handleRemoveItem = (item: CommonObject, index: number): void => {
    if (item.isTop ?? false) {
      message.warning('请先取消置顶，后删除该事项')
    } else {
      tooltipVisible.value && state.value.todoList.splice(index, 1)
    }
  }

  const handleRemoveAll = (): void => {
    state.value.todoList = state.value.todoList.filter((item: CommonObject) => {
      return !item.done
    })
  }

  const handleSticky = (item: CommonObject, index: number): void => {
    // console.log(index)

    // let realIndex = 0
    // state.value.todoList.forEach((itemTmp: CommonObject, indexTmp: number) => {
    //   if (itemTmp.id === item.id) realIndex = indexTmp
    // })
    // console.log(realIndex)

    if (item.isTop ?? false) {
      item.isTop = false
      lastTopIndex.value--
      const removeItem: CommonObject = state.value.todoList.splice(index, 1)[0]
      state.value.todoList.splice(lastTopIndex.value + 1, 0, removeItem)
    } else {
      const removeItem: CommonObject = state.value.todoList.splice(index, 1)[0]
      removeItem.isTop = true
      state.value.todoList.unshift(removeItem)
      lastTopIndex.value++
    }
  }

  return {
    UnDoCount,
    handleCheckbox,
    handleAddItem,
    handelTooltip,
    handleRemoveItem,
    handleRemoveAll,
    handleSticky,
  }
}
export default handleTodoList
