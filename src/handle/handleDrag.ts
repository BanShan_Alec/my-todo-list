import Sortable from 'sortablejs'
import { ref } from 'vue'

const sortableInstance = ref<CommonObject | null>(null)

const handleDrag = (state: CommonObject) => {
  const initDrag = (el: Element) => {
    // const dragList = [...state.value.todoList]
    // const dragList = toRaw(state.value.todoList)

    // 首先获取需要拖拽的dom节点
    // const elementList = document.querySelectorAll('.ant-list-items')[0]
    const GET_OPTIONS = () => {
      return {
        disabled: false, // 是否关闭拖拽
        // disabled: state.listType !== 'all', // 是否关闭拖拽
        // ghostClass: 'blue-background-class', // 拖拽样式
        dataIdAttr: 'row-key', // id绑定属性
        filter: '.topItem', // class类来控制不允许拖拽
        animation: 120, // 拖拽延时，效果更好看
        group: {
          // 是否开启跨表拖拽
          pull: false,
          put: false,
        },
        onMove: (e: CommonObject) => {
          // 取得代替的element
          const relatedElement = e.related
          // 判断element是否置顶 && 是在element前面插入
          const flag =
            relatedElement.getAttribute('class')?.indexOf('topItem') > -1 &&
            !e.willInsertAfter
          // 如果是，则return false 不允许拖拽
          return !flag
        },
        onEnd: async ({ newIndex, oldIndex }: CommonObject) => {
          if (oldIndex !== newIndex) {
            const val = state.value.todoList[oldIndex]
            // notice: 不应该在，hook里面直接修改，外面的数据tooList
            // 建议：外部传入修改的方法，updateTodoList
            state.value.todoList.splice(oldIndex, 1)
            state.value.todoList.splice(newIndex, 0, val)
            // 一定要绑定:key为唯一值，sortableJS是直接操作dom，但内部的vnode还没改变
            // state.value.todoList = dragList
            console.log(state.value.todoList)
          }
        },
        //   onEnd: async (e: CommonObject) => {
        //     console.log(e)
        //     const arr = sortable.toArray()
        //     console.log(sortable)
        //     // 这里主要进行数据的处理，拖拽实际并不会改变绑定数据的顺序，这里需要自己做数据的顺序更改
        //     if (e.oldIndex !== e.newIndex) {
        //       const newList: CommonObject[] = []
        //       arr.forEach((idStr: string) => {
        //         newList.push(
        //           state.value.todoList.find((item: CommonObject) => {
        //             return item?.id === parseInt(idStr)
        //           }),
        //         )
        //       })
        //       await nextTick()
        //       state.value.todoList = newList
        //       console.log(state.value.todoList)
        //     }
        //     if (e.oldIndex !== e.newIndex) {
        //       const listTmp = state.value.todoList
        //       listTmp.splice(e.newIndex, 0, listTmp.splice(e.oldIndex, 1)[0])
        //       await nextTick()
        //       state.value.todoList = listTmp
        //       console.log(state.value.todoList)
        //     }
        //   },
      }
    }
    // 然后创建sortable实例
    sortableInstance.value = Sortable.create(el, GET_OPTIONS())
  }

  return {
    initDrag,
    sortableInstance,
  }
}

export default handleDrag
