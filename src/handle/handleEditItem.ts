import { ref } from 'vue'
import handleTodoList from './handleTodoList'

const updateInfo = ref('')
const checkboxRefs = ref<AnyType[]>([])
const inputRefs = ref<AnyType[]>([])

const handleEditItem = (state: CommonObject) => {
  const handleDblclick = (item: CommonObject, index: number) => {
    updateInfo.value = item.info
    showInput(index)
    inputRefs.value[index].focus()
  }
  const handleBlur = (index: number) => {
    showCheckbox(index)
    updateInfo.value = ''
  }
  const handleEnter = (index: number) => {
    showCheckbox(index)
    updateTodoListItem(index, updateInfo.value)
    updateInfo.value = ''
  }
  const updateTodoListItem = (index: number, str: string) => {
    str = str.trim()
    str.length === 0
      ? handleTodoList(state).handleRemoveItem(state.value.todoList[index], index)
      : (state.value.todoList[index].info = str)
  }
  const showCheckbox = (index: number) => {
    if (checkboxRefs.value[index]) {
      checkboxRefs.value[index].style.display = 'inline-flex'
      setTimeout(() => (checkboxRefs.value[index].style.opacity = 1), 20)
    }
    if (inputRefs.value[index]) {
      inputRefs.value[index].$el.style.display = 'none'
      setTimeout(() => (inputRefs.value[index].$el.style.opacity = 0), 20)
    }
  }
  const showInput = (index: number) => {
    // console.log(checkboxRefs.value[index].style) // ref 指向的真实dom

    if (checkboxRefs.value[index]) {
      checkboxRefs.value[index].style.display = 'none'
      setTimeout(() => (checkboxRefs.value[index].style.opacity = 0), 20)
    }
    if (inputRefs.value[index]) {
      inputRefs.value[index].$el.style.display = 'inline-block'
      setTimeout(() => (inputRefs.value[index].$el.style.opacity = 1), 20)
    }
  }
  return {
    updateInfo,
    checkboxRefs,
    inputRefs,
    handleDblclick,
    handleBlur,
    handleEnter,
  }
}
export default handleEditItem
