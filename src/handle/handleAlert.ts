import { ref } from 'vue'

const visible = ref(false)
const title = ref('')
const okText = '确定'
const cancelText = '取消'

const handleAlert = (alertTitle: string, fn: () => void) => {
  title.value = alertTitle
  const show = (): void => {
    visible.value = true
  }
  const close = (): void => {
    visible.value = false
  }
  const confirm = (): void => {
    fn()
    close()
  }

  return {
    visible,
    title,
    okText,
    cancelText,
    show,
    close,
    confirm,
  }
}

export default handleAlert
