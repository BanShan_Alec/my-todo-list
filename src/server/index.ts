// ts 引入 js文件，需要在d.ts文件处声明
import myserver from '@/request/index'
// server module
import todolist from './module/todolist'
// 注册api
myserver.registerApi('todolist', todolist)

export default myserver
