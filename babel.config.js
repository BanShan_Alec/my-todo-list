const prodPlugins = []
if (process.env.NODE_ENV === 'production') {
  prodPlugins.push(['transform-remove-console', { exclude: ['error', 'warn'] }])
}

module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  plugins: [
    // 发布产品时候的插件数组
    ...prodPlugins,
    // 路由懒加载插件
    // '@babel/plugin-syntax-dynamic-import'
    // 组件，按需加载css
    [
      'import',
      {
        libraryName: 'ant-design-vue',
        libraryDirectory: 'es',
        style: 'css',
      },
    ],
  ],
}
