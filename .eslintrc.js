module.exports = {
  root: true,
  env: {
    node: true
  },
  globals: {
    CommonObject: true
  },
  extends: [
    'plugin:vue/vue3-essential',
    '@vue/standard',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    '@typescript-eslint/no-explicit-any': ['off'],
    // 方法名后跟1个空格
    'space-before-function-paren': 0,
    // 拖尾不允许逗号
    'comma-dangle': 0,
    // 强制const
    'prefer-const': 0,
  },
  overrides: [
    {
      // eslint无法识别d.ts的声明，typescript-eslint可以识别
      files: ['*.vue'],
      rules: {
        'no-undef': 'off',
      },
    },
  ],
}
