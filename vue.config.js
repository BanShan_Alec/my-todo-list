module.exports = {
  publicPath: './',

  chainWebpack: config => {
    // 发布模式
    config.when(process.env.NODE_ENV === 'production', config => {
      config
        .entry('app')
        .clear()
        .add('./src/main.ts')

      config.plugin('html').tap(args => {
        args[0].isProd = true
        return args
      })
    })

    // 开发模式
    config.when(process.env.NODE_ENV === 'development', config => {
      config
        .entry('app')
        .clear()
        .add('./src/main.ts')

      config.plugin('html').tap(args => {
        args[0].isProd = false
        return args
      })
    })
  },

  // babel 不编译node_modules
  transpileDependencies: false,
  // 生产环境，不生成SourceMap 源代码
  productionSourceMap: false
}
